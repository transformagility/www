import profiles from './profilesData';

// interface OptionsInterface {
//   who: string;
// }

// export const useProfile = (options: OptionsInterface) => {
export const useProfile = (who: String) => {
  if (who && profiles[who as keyof Object]) {
    // return profiles[options.who as keyof Object]
    return profiles[who as keyof Object]
  }
  return profiles
}
