import groundRules from './groundRulesData';

// interface OptionsInterface {
//   rule: string;
// }

export const useGroundRules = (rule: String) => {
  if (rule) {
    let result = groundRules.find(item => item.key === rule);
    if (result) {
      return result;
    }
  }
  return groundRules
}
