// If adding another profile, make sure to export it as per EOF

const groundRules = [
    {
        key: "contribute",
        title: "Contribute positively",
        tagline: "Turn up and do what you can each day toward goals",
        expanded: "Turn up and do what you can each day toward goals"
    },{
        key: "support",
        title: "Support each other",
        tagline: "Have each others backs and assist where you can",
        expanded: "Turn up and do what you can each day toward goals"
    },{
        key: "happy",
        title: "Go home happy",
        tagline: "Acknowledge the positive effect you've had each day",
        expanded: "Turn up and do what you can each day toward goals"
    }
]

export default groundRules;