// If adding another profile, make sure to export it as per EOF

const profiles: Object = {
    daniel: {
        meta: {
            _path: "/profile/daniel",
            _status: "active",
            _joined: "15/04/2023"
        },
        handle: "Squibler",
        name: {
            given: "Daniel",
            family: "Smith",
            full: "Daniel Smith"
        },
        position: "Agile Coach",
        avatar: "/images/profiles/daniel.jpg",
        headline: "Agile Coach, Technologist, Engineer, MBA Candidate",
        info: [
            {key: 'Location', value: 'Brisbane'},
            {key: 'Experience', value: '15+ Years'},
            {key: 'Availability', value: '4 Weeks'},
            {key: 'Relocation', value: 'Yes'},
        ],
        contact: {
            email: "potsed@gmail.com",
        },
        social: [
            {
                service: "email",
                url: "mailto:potsed@gmail.com"
            },
            {
                service: "linkedin",
                url: "https://danielsmith.top"
            }
        ],
        bio: "Change, by its nature, pushes us out of our natural comfort zones. In a business world where ongoing, and rapid, transformation is critical to survival, true agile working should be BAU. I welcome connections. Contact me via LinkedIn."
    },
    ishan: {
        meta: {
            _path: "/profile/ishan",
            _status: "active",
            _joined: "15/04/2023"
        },
        handle: "IshanR",
        name: {
            given: "Ishan",
            family: "Roy",
            full: "Ishan Roy"
        },
        position: "",
        avatar: "/images/profiles/ishan.jpg",
        headline: "I like frameworks but I love people",
        info: [
            {key: 'Location', value: 'Brisbane'},
            {key: 'Experience', value: '10+ Years'},
            {key: 'Availability', value: '4 Weeks'},
            {key: 'Relocation', value: 'No'},
        ],
        contact: {
            email: "ishanroy2010@outlook.com",
        },
        social: [
            {
                service: "email",
                url: "mailto:ishanroy2010@outlook.com"
            },
            {
                service: "linkedin",
                url: "https://www.linkedin.com/in/ishan-roy-7a4696a2/"
            }
        ],
        bio: "Ishan is an experienced Agile coach and consultant with a passion for people over frameworks. He has extensive knowledge and expertise in various Agile frameworks and methodologies, including Scrum, Kanban, and SAFe. He excels in facilitating Agile transformations and coaching teams and organisations towards Agile maturity."
    }
}

export default profiles;