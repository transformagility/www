// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  ssr: true,
  css: ["~/assets/styles/app.css"],
  postcss: {
    plugins: {
      "postcss-simple-vars": {},
      "postcss-nesting": {},
      "postcss-apply": {},
    },
  },
  dir: {
    static: "static",
    public: "static",
  },
  nitro: {
    // preset: 'github',
    // output: {
    //     dir: 'output',
    //     serverDir: 'output/server',
    //     publicDir: 'output/public'
    // }
  },
  experimental: {
    payloadExtraction: false,
  },
  app: {
    pageTransition: true,
    layoutTransition: true,
    head: {
      title: "Articles focused on agile leadership and mindset",
      htmlAttrs: { lang: "en-AU", class: "dark" },
      script: [
        {
          defer: true,
          src: "https://app.tinyanalytics.io/pixel/Zu2udHFXu4qNAfI3",
        },
        {
          hid: "UmaniAnalyticsPrivacyAware",
          async: true,
          defer: true,
          src: "https://analytics.umami.is/script.js",
          "data-website-id": "58126111-89b9-444f-be2d-08426a3feaaf",
          "data-domains": "transformagility.com.au",
          "data-do-not-track": true,
        },
        {
          hid: "DarkModeToggle",
          innerHTML: `
                if (localStorage.getItem('color-theme') === 'dark' || (!('color-theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
                    document.documentElement.classList.add('dark');
                } else {
                    document.documentElement.classList.remove('dark')
                }`,
          type: "text/javascript",
        },
      ],
      meta: [
        { charset: "utf-8" },
        { name: "HandheldFriendly", content: "True" },
      ],
      link: [
        {
          hid: "canonical",
          rel: "canonical",
          href: "https://transformagility.com.au",
        },
      ],
    },
  },
  router: {
    options: {
      linkActiveClass: "active",
      linkExactActiveClass: "exact-active",
    },
  },
  content: {
    documentDriven: true,
    markdown: {
      remarkPlugins: ["remark-emoji", "remark-rehype"],
    },
  },
  modules: [
    "nuxt-content-assets",
    "@unocss/nuxt",
    "@vueuse/nuxt",
    "@nuxt/content",
  ],
  unocss: {},
  build: {},
  vite: {
    define: {
      "process.env.DEBUG": false,
    },
  },
});
