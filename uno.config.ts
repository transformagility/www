import {
    defineConfig,
    presetUno,
    presetTypography,
    presetIcons,
    presetAttributify,
    presetWebFonts,
} from 'unocss'
import { presetHeroPatterns } from '@julr/unocss-preset-heropatterns'
import transformerDirectives from '@unocss/transformer-directives'

export default defineConfig({
    transformers: [
        transformerDirectives(),
    ],
    presets: [
        presetUno(),
        presetTypography({}),
        presetIcons({ /* options */ }),
        presetAttributify({ /* preset options */ }),
        presetWebFonts({
            provider: "google",
            fonts: {
                sans: 'Ubuntu',
                mono: ['Fira Code', 'Fira Mono:400,700'],
            },
        }),
        presetHeroPatterns(),
    ],

})