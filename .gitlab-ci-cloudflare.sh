#! /bin/sh
# Variables defined in gitlab group transformagility/www > settings > ci/cd
# $CLOUDFLARE_IDENTITY = site identifier
# $CLOUDFLARE_PURGE_KEY = secret bearer token
curl --location 'https://api.cloudflare.com/client/v4/zones/'$CLOUDFLARE_IDENTITY'/purge_cache' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer '$CLOUDFLARE_PURGE_KEY \
--data '{
  "purge_everything": true
}'