/* @unocss-include */
export let icons = {
    social: {
        email: "i-ph-envelope-simple-light",
        linkedin: "i-ph-linkedin-logo-light"
    }
}

export let social = icons.social;