# TransformAgility.com.au

[![pipeline status](https://gitlab.com/transformagility/www/badges/main/pipeline.svg)](https://gitlab.com/transformagility/www/-/commits/main)

We believe that character will define organisational success as we move into the age of Machine Intelligence. Particularly the characteristics of resilience, resourcefulness, innovativeness, agility, humanity, and integrity. So we collaborate with people who have innovative ideas and who choose to help define the future of business operating systems and bring it to life today. We do that by creating our open source TagOS Framework which takes a mindset and people first approach and which continually improves as wise people contribute.

For more information please visit [transformagility.com.au](https://transformagility.com.au)
