# [0.5.0](https://gitlab.com/transformagility/www/compare/v0.4.1...v0.5.0) (2023-06-01)


### Features

* **darkmode:** darkmode toggle ([61846f2](https://gitlab.com/transformagility/www/commit/61846f2c58d18c841cba2592c34e1def192ff157))

## [0.4.1](https://gitlab.com/transformagility/www/compare/v0.4.0...v0.4.1) (2023-05-12)


### Bug Fixes

* **guide:** scrum Guide Timeboxes ([0f22176](https://gitlab.com/transformagility/www/commit/0f221762b56aca625d81ded66dd0caeca7663845))

# [0.4.0](https://gitlab.com/transformagility/www/compare/v0.3.0...v0.4.0) (2023-05-12)


### Features

* article updates ([aaa778a](https://gitlab.com/transformagility/www/commit/aaa778afe7f6519e1e0368be12b48713ccdeac24))

# [0.3.0](https://gitlab.com/transformagility/www/compare/v0.2.0...v0.3.0) (2023-05-12)


### Features

* article updates ([34b9c01](https://gitlab.com/transformagility/www/commit/34b9c0124a03db6f701da27acd8efd69fd7d71f9))

# [0.2.0](https://gitlab.com/transformagility/www/compare/v0.1.2...v0.2.0) (2023-05-12)


### Features

* **stakeholder article:** Stakeholder Article ([3b6fc05](https://gitlab.com/transformagility/www/commit/3b6fc0509efed14bde9e3dccc5caa13783686d76))

## [0.1.2](https://gitlab.com/transformagility/www/compare/v0.1.1...v0.1.2) (2023-05-10)


### Bug Fixes

* CI/CD only on main ([bcf88f9](https://gitlab.com/transformagility/www/commit/bcf88f9ec3080c19389c786c15cce8cd46f39ce0))

## [0.1.2](https://gitlab.com/transformagility/www/compare/v0.1.1...v0.1.2) (2023-05-10)


### Bug Fixes

* CI/CD only on main ([bcf88f9](https://gitlab.com/transformagility/www/commit/bcf88f9ec3080c19389c786c15cce8cd46f39ce0))

## [0.1.1](https://gitlab.com/transformagility/www/compare/v0.1.0...v0.1.1) (2023-05-09)


### Bug Fixes

* **ci:** removed separate section for semver to speed up deploy on a single image ([fc8aa58](https://gitlab.com/transformagility/www/commit/fc8aa589c4082e705af2de25ca57531ee5e198cb))

# [0.1.0](https://gitlab.com/transformagility/www/compare/v0.0.2...v0.1.0) (2023-05-09)


### Features

* **framework:** added in a test for the ci-cd pipeline ([76b9023](https://gitlab.com/transformagility/www/commit/76b9023ea6f8b83f054e8cec3ea7205e15f368f0))

## [0.0.2](https://gitlab.com/transformagility/www/compare/v0.0.1...v0.0.2) (2023-05-09)


### Bug Fixes

* **configuration:** added repository to the package.json for semver ([59c5697](https://gitlab.com/transformagility/www/commit/59c56976c6dc2f7e2cc53d34b87becabe13708f2))
